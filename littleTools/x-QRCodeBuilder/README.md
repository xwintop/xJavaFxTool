QRCodeBuilder 二维码生成工具

#### 项目简介：
QRCodeBuilder是使用javafx开发的一款二维码生成工具，可快速生成二维码，可截图识别图片二维码。

#### 功能特色：
- 支持前景色、背景色、容错等级、边距、编码等条件自定义
- 支持添加图片logo，支持logo大小自定义
- 能截图识别图片或桌面图片中的二维码

**xJavaFxTool交流QQ群：== [387473650(此群已满)](https://jq.qq.com/?_wv=1027&k=59UDEAD) 请加群②[1104780992](https://jq.qq.com/?_wv=1027&k=bhAdkju9) ==**

#### 环境搭建说明：
- 开发环境为jdk1.8，基于maven构建
- 使用eclipase或Intellij Idea开发(推荐使用[Intellij Idea](https://www.jetbrains.com/?from=xJavaFxTool))
- 该项目为javaFx开发的实用小工具集[xJavaFxTool](https://gitee.com/xwintop/xJavaFxTool)的插件。
- 本项目使用了[lombok](https://projectlombok.org/),在查看本项目时如果您没有下载lombok 插件，请先安装,不然找不到get/set等方法
- 依赖的[xcore包](https://gitee.com/xwintop/xcore)已上传至git托管的maven平台，git托管maven可参考教程(若无法下载请拉取项目自行编译)。[教程地址：点击进入](http://blog.csdn.net/u011747754/article/details/78574026)

![二维码生成工具.png](images/二维码生成工具.png)
![二维码生成工具.gif](images/二维码生成工具.gif)

#### 版本记录
- 1.0.0
  1. 完成基本功能配置
- 1.0.1  20200419
  1. 优化界面布局


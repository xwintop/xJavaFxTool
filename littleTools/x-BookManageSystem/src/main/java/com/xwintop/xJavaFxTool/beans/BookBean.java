package com.xwintop.xJavaFxTool.beans;

import lombok.Data;

@Data
public class BookBean {
    private int bookId;
    private String bookName;
    private String bookAuthor;
    private String bookAuthorSex;
    private float bookPrice;
    private String bookDescription;
    private String bookTypeId;
}
